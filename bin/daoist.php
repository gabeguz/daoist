<?php
/*
 * $Id$ 
 *
 * daoist
 */

// DAOIST SPECIFIC PATHS
define('CURRENT_PATH', realpath(__DIR__));
define('ETC', realpath(CURRENT_PATH . '/../etc'));
define('BIN', realpath(CURRENT_PATH . '/../bin'));
define('SRC', realpath(CURRENT_PATH . '/../src'));
define('VARS', realpath(CURRENT_PATH . '/../var'));
define('TEMPLATES', realpath(SRC . '/templates'));
define('CONFIG_FILE', ETC . '/daoist.ini');

// FOR PROJECTS
define('ROOT', '/daoist');
define('GENERATED', '/classes');
define('XML', '/xml');
define('BASE', '/base');
define('CETC', '/etc');
define('HTDOCS', '/htdocs');

// read config file
$projects = parse_ini_file(CONFIG_FILE, TRUE);

foreach($projects as $conf) {
    // client paths
    $root = $conf['root'] . ROOT . '/' . $conf['name'];
    $generated = $root . GENERATED;
    $xml = $root . XML;
    $base = $generated . BASE;
    $etc = $root . CETC;
    $htdocs = $root . HTDOCS;

    if(!file_exists($xml)) {
        mkdir($xml, 0750, TRUE);
    }
    if(!file_exists($base)) {
        mkdir($base, 0750, TRUE);
    }
    if(!file_exists($etc)) {
        mkdir($etc, 0750, TRUE);
    }
    if(!file_exists($htdocs)) {
        mkdir($htdocs, 0750, TRUE);
    }

    $db = new mysqli($conf['host'], $conf['user'], $conf['pass']);
    if ($db->select_db($conf['name'])) {
        $sql = "show tables";
        $result = $db->query($sql);
        if(!$result) {
            printf("Error: %s\n", $db->error);
            exit(1);
        }
    } else {
        printf("Error: %s\n", $db->error);
        exit(1);
    }

    $pxml = new SimpleXMLElement("<project></project>");
    while ($row = $result->fetch_array()) {
        $sql = "desc " . $row[0];
        $tableinfo = $db->query($sql);
        $txml = new SimpleXMLElement("<table></table>");
        $txml->addAttribute('name', htmlspecialchars($row[0]));
        $txml->addAttribute('type', 'mysql');
        $txml->addAttribute('dbname', $conf['name']);
        $txml->addAttribute('host', $conf['host']);
        $txml->addAttribute('port', $conf['port']);
        $txml->addAttribute('user', $conf['user']);
        $txml->addAttribute('pass', $conf['pass']);
        $txml->addAttribute('cset', $conf['cset']);

        $pxml->addChild('table', htmlspecialchars($row[0]));
        while($inforow = $tableinfo->fetch_assoc()) {
            $field = $txml->addChild('field');
            $field->addChild('name', htmlspecialchars($inforow['Field']));
            $field->addChild('type', htmlspecialchars($inforow['Type']));
            $field->addChild('null', htmlspecialchars($inforow['Null']));
            $field->addChild('key', htmlspecialchars($inforow['Key']));
            $field->addChild('default', htmlspecialchars($inforow['Default']));
            $field->addChild('extra', htmlspecialchars($inforow['Extra']));
        }

        // build php base transfer objects:
        $xslt = new SimpleXMLElement(
                        TEMPLATES . '/phpDataTransferObjectBase.xslt', 
                        NULL, TRUE);
        $xsltp = new XSLTProcessor(); 
        $xsltp->importStyleSheet($xslt);
        $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($txml) . "\n?" . ">";
        file_put_contents($base . '/' . $row[0] . '_dto_base.php', $phpstring);
        
        // build php transfer objects:
        if(!file_exists($generated . '/' . $row[0] . '_dto.php')) {
            $xslt = new SimpleXMLElement(
                            TEMPLATES . '/phpDataTransferObject.xslt', 
                            NULL, TRUE);
            $xsltp = new XSLTProcessor(); 
            $xsltp->importStyleSheet($xslt);
            $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($txml) . "\n?" . ">";
            file_put_contents($generated . '/' . $row[0] . '_dto.php', $phpstring);
        }
        
        // build php base data access objects: 
        $xslt = new SimpleXMLElement(
                        TEMPLATES . '/phpDataAccessObjectBase.xslt', 
                        NULL,                     TRUE);
        $xsltp->importStyleSheet($xslt);
        $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($txml) . "\n?" . ">";
        file_put_contents($base . '/' . $row[0] . '_dao_base.php', $phpstring);
        
        // build php data access objects: 
        if(!file_exists($generated . '/' . $row[0] . '_dao.php')) {
            $xslt = new SimpleXMLElement(
                            TEMPLATES . '/phpDataAccessObject.xslt', 
                            NULL,                     TRUE);
            $xsltp->importStyleSheet($xslt);
            $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($txml) . "\n?" . ">";
            file_put_contents($generated . '/' . $row[0] . '_dao.php', $phpstring);
        }
        

        // build php data transfer object web access pages: 
        $xslt = new SimpleXMLElement(
                        TEMPLATES . '/phpDataTransferObjectWebGetSet.xslt', 
                        NULL,                     TRUE);
        $xsltp->importStyleSheet($xslt);
        $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($txml) . "\n?" . ">";
        file_put_contents($htdocs . '/' . $row[0] . '.php', $phpstring);
        
        // build js data transfer object web display pages: 
        $xslt = new SimpleXMLElement(
                        TEMPLATES . '/jsDataTransferObjectList.xslt', 
                        NULL,                     TRUE);
        $xsltp->importStyleSheet($xslt);
        $jsstring = $xsltp->transformToXML($txml);
        file_put_contents($htdocs . '/' . $row[0] . '.js', $jsstring);
        // write out tables xml file
        // Specify configuration
        $config = array('indent'         => true,
                        'input-xml'      => true,
                        'wrap'           => 80);
        $tidy = new tidy;
        $tidy->parseString($txml->asXML(), $config, 'utf8');
        $tidy->cleanRepair();
        file_put_contents($xml . '/' . $row[0] . '.xml', tidy_get_output($tidy));
    }

    // build php data access object factory:
    $xslt = new SimpleXMLElement(
                    TEMPLATES . '/phpDataAccessObjectFactory.xslt', 
                    NULL, TRUE);
    $xsltp->importStyleSheet($xslt);
    $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($pxml) . "\n?" . ">";
    file_put_contents($generated . '/dao_factory.php', $phpstring);

    // build php data transfer object factory:
    $xslt = new SimpleXMLElement(
                    TEMPLATES . '/phpDataTransferObjectFactory.xslt', 
                    NULL, TRUE);
    $xsltp->importStyleSheet($xslt);
    $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($pxml) . "\n?" . ">";
    file_put_contents($generated . '/dto_factory.php', $phpstring);

    // build php data access object interface: 
    $xslt = new SimpleXMLElement(
                    TEMPLATES . '/phpDataAccessObjectInterface.xslt', 
                    NULL, TRUE);
    $xsltp->importStyleSheet($xslt);
    $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($pxml) . "\n?" . ">";
    file_put_contents($base . '/dao_interface.php', $phpstring);

    // build php data access object mysql abstract class
    $xslt = new SimpleXMLElement(
                    TEMPLATES . '/phpDataAccessObjectMysqlAbstract.xslt', 
                    NULL, TRUE);
    $xsltp->importStyleSheet($xslt);
    $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($pxml) . "\n?" . ">";
    file_put_contents($base . '/dao_mysql.php', $phpstring);

    // build php data transfer object interface
    $xslt = new SimpleXMLElement(
                    TEMPLATES . '/phpDataTransferObjectInterface.xslt', 
                    NULL, TRUE);
    $xsltp->importStyleSheet($xslt);
    $phpstring = "<" . "?" . "php\n" . $xsltp->transformToXML($pxml) . "\n?" . ">";
    file_put_contents($base . '/dto_interface.php', $phpstring);

    // build project config file
    $xslt = new SimpleXMLElement(
                    TEMPLATES . '/conf.xslt', NULL, TRUE);
    $xsltp->importStyleSheet($xslt);
    file_put_contents($etc . '/conf.ini', $xsltp->transformToXML($txml));
}

?>
