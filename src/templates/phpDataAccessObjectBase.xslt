<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match="/">
/* 
 * $Id$
 *
 */
include_once 'dao_interface.php';
class <xsl:value-of select="table/@name" />_dao_base implements dao_interface {

    /*
     * DAO Constants - ALL SQL statements relating to this table belong here.
     */
    const create_stmt = "<xsl:call-template name="insert-sql" />
                        ";

    const read_stmt = "<xsl:call-template name="select-sql" />
                        ";

    const update_stmt = "<xsl:call-template name="update-sql" />
                        ";

    const delete_stmt = "<xsl:call-template name="delete-sql" />
                        ";

    /*
     * Custom Constants - Add your own SQL for specialized DB interactions here:
     */

    public function __construct($db) {
        $this->dbh = $db->dbh;
    }

    /*
     * DAO Interface Implementation
     */
    public function create($o) {
        <xsl:call-template name="create-function" />
    }

    public function read($key) {
        <xsl:call-template name="read-function" />
    }

    public function update($o) {
        <xsl:call-template name="update-function" />
    }

    public function delete($o) {
        <xsl:call-template name="delete-function" />
    }

}

</xsl:template>

<xsl:template name="insert-sql">
        INSERT 
        INTO <xsl:value-of select="table/@name" /> ( <xsl:apply-templates select="table/field[extra !='auto_increment' and default!='CURRENT_TIMESTAMP']/name" /> ) 
        VALUES ( <xsl:apply-templates select="table/field[key!='PRI' and extra!='auto_increment' and default!='CURRENT_TIMESTAMP']/name" mode="qmark"/> )
</xsl:template>

<xsl:template name="select-sql">
        SELECT <xsl:apply-templates select="table/field/name" /> 
        FROM <xsl:value-of select="table/@name" /> 
        WHERE <xsl:apply-templates select="table/field[key='PRI']/name" /> = ?
</xsl:template>

<xsl:template name="update-sql">
        UPDATE <xsl:value-of select="table/@name" /> 
        SET <xsl:apply-templates select="table/field[extra!='auto_increment' and default!='CURRENT_TIMESTAMP']/name" mode="name_equals_qmark" /> 
        WHERE <xsl:apply-templates select="table/field[key='PRI']/name" /> = ?
</xsl:template>

<xsl:template name="delete-sql">
        DELETE 
        FROM <xsl:value-of select="table/@name" /> 
        WHERE <xsl:apply-templates select="table/field[key='PRI']/name" /> = ?
</xsl:template>

<xsl:template name="create-function">
        if($stmt = $this->dbh->prepare(self::create_stmt)) {
            $stmt->bind_param("<xsl:apply-templates select="table/field[extra !='auto_increment' and default != 'CURRENT_TIMESTAMP']/type"/>", <xsl:apply-templates select="table/field[extra != 'auto_increment' and default != 'CURRENT_TIMESTAMP']/name" mode="object_to_name"/>);
            $stmt->execute();
            if($stmt->errno != 0) {
                throw new exception("Error in SQL statement " . $stmt->error);
            }
        } else {
            throw new exception("Database error " . $this->dbh->error);
        }
        <!-- different behavior if primary key is AUTO_INCREMENT or not -->
        <xsl:choose>
            <xsl:when test="table/field[extra = 'auto_increment']"> 
        <xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/> = $stmt->insert_id;
        $stmt->close();
        return(<xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/> > 0 ? <xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/> : false);
            </xsl:when>
            <xsl:otherwise>
        $affected = $this->dbh->affected_rows;
        $stmt->close();
        return($affected > 0 ? <xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/> : false);
            </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:template name="read-function">
        if($stmt = $this->dbh->prepare(self::read_stmt)) {
            $stmt->bind_param("<xsl:apply-templates select="table/field[key='PRI']/type"/>", $key);
            $stmt->execute();
            $o = new <xsl:value-of select="table/@name"/>_dto();
            $stmt->bind_result(<xsl:apply-templates select="table/field/name" mode="object_to_name"/>);
            $stmt->fetch();
            if($stmt->errno != 0) {
                throw new exception("Error in SQL statement " . $stmt->error);
            }
        } else {
            throw new exception("Database error " . $this->dbh->error);
        }
        $stmt->close();
        return(<xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/> > 0 ? $o : false);
</xsl:template>

<xsl:template name="update-function">
        if($stmt = $this->dbh->prepare(self::update_stmt)) {
            $stmt->bind_param(<xsl:apply-templates select="table/field[extra !='auto_increment' and default != 'CURRENT_TIMESTAMP']/type"/><xsl:apply-templates select="table/field[key='PRI']/type"/>, <xsl:apply-templates select="table/field[key != 'PRI' and extra != 'auto_increment' and default != 'CURRENT_TIMESTAMP']/name" mode="object_to_name"/>, <xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/>);
            $stmt->execute();
            if($stmt->errno != 0) {
                throw new exception("Error in SQL statement " . $stmt->error);
            }
        } else {
            throw new exception("Database error " . $this->dbh->error);
        }
        $stmt->close();
        return true;
</xsl:template>

<xsl:template name="delete-function">
        if($stmt = $this->dbh->prepare(self::delete_stmt)) {
            if(<xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/> > 0) {
                $stmt->bind_param('<xsl:apply-templates select="table/field[key='PRI']/type"/>', <xsl:apply-templates select="table/field[key='PRI']/name" mode="object_to_name"/>);
                $stmt->execute();
                if($stmt->errno != 0) {
                    throw new exception("Error in SQL statement " . $stmt->error);
                }
            } else {
                throw new exception("Invalid <xsl:apply-templates select="table/field[key='PRI']/name"/>");
            }
            $stmt->close();

        } else {
            throw new exception("Database error " . $this->dbh->error);
        }
        return true;
</xsl:template>

<!-- XSLT Primitives -->
<xsl:template match="name">
    <!-- if it's the last one in the set, don't append a comma -->
    <xsl:choose>
        <xsl:when test="position() = last()"><xsl:value-of select="."/></xsl:when>
        <xsl:otherwise><xsl:value-of select="."/>, </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="name" mode="qmark">
    <!-- if it's the last one in the set, don't append a comma -->
    <xsl:choose>
        <xsl:when test="position() = last()">?</xsl:when>
        <xsl:otherwise>?, </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="name" mode="name_equals_qmark">
    <!-- if it's the last one in the set, don't append a comma -->
    <xsl:choose>
        <xsl:when test="position() = last()"><xsl:value-of select="." /> = ? </xsl:when>
        <xsl:otherwise><xsl:value-of select="." /> = ?, </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="name" mode="object_to_name">
    <!-- if it's the last one in the set, don't append a comma -->
    <xsl:choose>
        <xsl:when test="position() = last()">$o-><xsl:value-of select="." /></xsl:when>
        <xsl:otherwise>$o-><xsl:value-of select="." />, </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="type">
    <xsl:choose>
        <xsl:when test="contains(., 'int')">i</xsl:when>
        <xsl:otherwise>s</xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
