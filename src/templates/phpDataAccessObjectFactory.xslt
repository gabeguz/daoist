<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match = "/">
class dao_factory {

    static function get($o) {
        $name = str_replace('_dto', '', get_class($o));
        switch($name) {
            <xsl:apply-templates match="table" />
            default: 
                throw new exception("No DAO found for type $name");
                break;
        }
        return $dao;
    }
}
</xsl:template>

<xsl:template match="table">
            case "<xsl:value-of select="." />":
            include_once('<xsl:value-of select="."/>_dao.php');
            $dao = new <xsl:value-of select="."/>_dao();
            break;
</xsl:template>

</xsl:stylesheet>
