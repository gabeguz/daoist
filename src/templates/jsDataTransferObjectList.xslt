<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match="/">

<!-- Template for creating a YUI based display for a given table. -->

/*
 *  $Id$
 */

// Instantiate and configure YUI Loader: 
(function() {
    var loader = new YAHOO.util.YUILoader({
        require: ["datasource", "datatable", "connection", "json", "event", "logger", "button"],
        loadOptional: true,
        combine: true,
        filter: "min",
        allowRollup: true,
        onSuccess: function() {
            /* DATA SOURCE */
            // add the sqlDate, parser to YUI: 
            YAHOO.util.DataSource.Parser['sqlDate'] = function (oData) {
                var parts = oData.split(' ');
                var datePart = parts[0].split('-');
                if (parts.length > 1) {
                    var timePart = parts[1].split(':');
                    return new Date(datePart[0],datePart[1]-1,datePart[2],timePart[0],timePart[1],timePart[2]);
                } else {
                    return new Date(datePart[0],datePart[1]-1,datePart[2]);
                }
            };

            // instantiate our data source:
            var myDataSource = new YAHOO.util.XHRDataSource("/<xsl:value-of select="table/@name"/>.php");
            myDataSource.responseType = YAHOO.util.XHRDataSource.TYPE_JSON;

            // define the fields we expect to recieve
            myDataSource.responseSchema = {
                resultsList:"data",
                fields: [<xsl:apply-templates />]
            };

            /* DATA TABLE */
            var myConfigs = {
                    paginator : new YAHOO.widget.Paginator({
                        rowsPerPage : 40
                    }),
                    sortedBy:{
                        key:'templatename',
 dir:YAHOO.widget.DataTable.CLASS_ASC
                    },
                    caption:"Renewal Notices",
                    summary:"List of Renewal Notices"
            };

            // setup a 'before parse data' function
            myDataSource.doBeforeParseData = function (oRequest, oFullResponse, oCallback) {

                if (oFullResponse.replyCode) {
                    if (oFullResponse.replyCode > 299) {
                        alert(oFullResponse.replyText);
                        return {};
                    }
                } else {
                    alert(oFullResponse);
                }
                return oFullResponse;
            };

            // create a custom formatter for template editing
            myURLFormatter = function(elCell, oRecord, oColumn, oData) {
                var templateName = oData;
                var id = oRecord.getData("id");
                elCell.innerHTML = "&lt;a href=\"/admin/renewalnotices.php?action=edit&amp;id=" + id + "\">" + templateName + "&lt;/a>";
                //elCell.innerHTML = "hi.";
            };

            YAHOO.widget.DataTable.Formatter.myURL = myURLFormatter;

            // create a custom formatter for the id display, add some functional
            // buttons (edit/delete)
            myKeyFormatter = function(elCell, oRecord, oColumn, oData) {
                var id = oData;
                elCell.innerHTML = "&lt;a href=\"/admin/renewalnotices.php?action=delete&amp;id=" + id + "\">delete&lt;/a>&lt;a href=\"/admin/renewalnotices.php?action=edit&amp;id=" + id + "\">edit&lt;/a>";
            }

            YAHOO.widget.DataTable.Formatter.myId = myIdFormatter;
// define the columns
            var myColumnDefs = [
            <xsl:apply-templates />
            ];

            // instantiate our data table:
            var myDataTable = new YAHOO.widget.DataTable ("tablecontainer", myColumnDefs, myDataSource, myConfigs);
            // create a button: 
            var oButton = new YAHOO.widget.Button({
                id: "addnewbutton",
                type: "submit",
                label: "Add New",
                container: "buttoncontainer"
            });
        }
    });

    // Load the files using the insert() method. 
    loader.insert();
})();
</xsl:template>

<xsl:template match="table/field">
    <xsl:choose>
        <xsl:when test="position() = last()">
                {key:"<xsl:value-of select="name"/>", label:"<xsl:value-of select="name"/>",abbr:"<xsl:value-of select="name"/>",sortable:true,formatter:"<xsl:value-of select="type"/>"}
        </xsl:when>
        <xsl:otherwise>
                {key:"<xsl:value-of select="name"/>", label:"<xsl:value-of select="name"/>",abbr:"<xsl:value-of select="name"/>",sortable:true,formatter:"<xsl:value-of select="type"/>"},
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>
<!-- XSLT Primitives -->
<xsl:template match="name">
    <!-- if it's the last one in the set, don't append a comma -->
    <xsl:choose>
        <xsl:when test="position() = last()"><xsl:value-of select="."/></xsl:when>
        <xsl:otherwise><xsl:value-of select="."/>, </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
