<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match = "/">
/* 
 * $Id$
 *
 */
class dto_factory {

    static function get($name) {
        switch($name) {
            <xsl:apply-templates match="table" />
            default: 
                throw new exception("No DTO found for type $name");
                break;
        }
        return $dto;
    }
}
</xsl:template>

<xsl:template match="table">
            case "<xsl:value-of select="." />":
            include_once('<xsl:value-of select="."/>_dto.php');
            $dto = new <xsl:value-of select="."/>_dto();
            break;
</xsl:template>

</xsl:stylesheet>
