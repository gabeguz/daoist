<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match="/">

/* 
 * $Id$
 *
 * <xsl:value-of select="table/@name" /> Web Resource
 */

include realpath(__DIR__) . '/../classes/dao_factory.php';
include realpath(__DIR__) . '/../classes/dto_factory.php';
$dto = dto_factory::get('<xsl:value-of select="table/@name"/>');
$dao = dao_factory::get($dto);

switch($_SERVER['REQUEST_METHOD']) {
    case "GET":
        if(isset($_GET['key']) &amp;&amp; strlen($_GET['key']) > 0) {
            $key = $_GET['key'];
            $dto = $dao->read($key);
            if($dto-><xsl:value-of select="table/field[key = 'PRI']/name"/>) {
                $web_response = array('Status'=>'Ok',
                                    'Payload'=>$dto);
            } else {
                $web_response = array('Status'=>'Error',
                                    'Message'=>'Invalid key');
            }
        }        
        break;
    case "PUT":
    case "DELETE":
    case "POST":
        if(isset($_POST['key']) &amp;&amp; strlen($_POST['key']) > 0) {
            foreach($dto as $element) {
                $dto->$element = $_POST['$element'];
            }
            $dao->update($dto);
        }
        break;
}



echo json_encode($web_response);

</xsl:template>
</xsl:stylesheet>
