<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match="/">
/* 
 * $Id$
 *
 */
include_once 'base/<xsl:value-of select="table/@name" />_dao_base.php';
class <xsl:value-of select="table/@name" />_dao extends <xsl:value-of select="table/@name" />_dao_base {

    /*
     * Custom DAO Constants - Add your own SQL for specialized DB interactions 
     *  here:
     */


    /*
     * Custom DAO methods - Add your own code for interacting w/the above SQL
     *  here:
     */
}

</xsl:template>

</xsl:stylesheet>
