<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match="/">
/*
 * $Id$
 *
 */

interface dao_interface {
   public function create($object);
   public function read($uri);
   public function update($object);
   public function delete($object);
}

</xsl:template>
</xsl:stylesheet>
