<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match="/">
/*
 * $Id$
 */

class dao_mysql extends Mysqli {

    public $dbh;
    private $conf = array();

    public function __construct($config) {

        $this->conf = $config;
        $this->dbh  = new mysqli($this->conf['host'], $this->conf['user'], 
                                 $this->conf['pass'], $this->conf['name']);
        /* check connection */
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        $this->dbh->set_charset('utf8');
        //printf("Host information: %s\n", $this->dbh->host_info);

    }

    public function __destruct() {
        $this->dbh->close();
    }
}
</xsl:template>
</xsl:stylesheet>
