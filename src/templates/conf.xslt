<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="text" omit-xml-declaration="yes" />

<xsl:template match="/">
;$Id$
; conf.ini
; configuration file for daoist
[development]
type=<xsl:value-of select="table/@type"/>
host=<xsl:value-of select="table/@host"/>
port=<xsl:value-of select="table/@port"/>
name=<xsl:value-of select="table/@dbname"/>
user=<xsl:value-of select="table/@user"/>
pass="<xsl:value-of select="table/@pass"/>"
cset=<xsl:value-of select="table/@cset"/>

[production]
type=<xsl:value-of select="table/@type"/>
host=<xsl:value-of select="table/@host"/>
port=<xsl:value-of select="table/@port"/>
name=<xsl:value-of select="table/@dbname"/>
user=<xsl:value-of select="table/@user"/>
pass="<xsl:value-of select="table/@pass"/>"
cset=<xsl:value-of select="table/@cset"/>
</xsl:template>
</xsl:stylesheet>
